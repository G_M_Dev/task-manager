const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        from: 'majdoub.ghazi@gmail.com',
        to: email,
        subject: 'Welcome to Task Manager App!',
        text: `Welcome to Task Manager, ${name}. Let me know how you get along with the App.`
    })
}

const sendByeEmail = (email, name) => {
    sgMail.send({
        from: 'majdoub.ghazi@gmail.com',
        to: email,
        subject: 'Sorry to see you go!',
        text: `${name}, the Task Manager App team is sorry to see you go... Please share with us your suggestions to improve our services!`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendByeEmail
}