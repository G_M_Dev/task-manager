const request = require('supertest')
const app = require('../src/app')
const Task = require('../src/models/task')
const { userTwo, taskOne, userOne, setupDb } = require('./fixtures/db')

beforeEach(setupDb)

test('Should create task for user', async () => {
    const res = await request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: 'From my test'
        })
        .expect(201)
    const task = await Task.findById(res.body._id)
    expect(task).not.toBeNull()
    expect(task.completed).toEqual(false)
})

test('Should get tasks of authenticated user', async () => {
    const res = await request(app)
        .get('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .expect(200)
    
    expect(res.body.length).toBe(2)
})

test('Should prevent user to delete another user task', async () => {
    await request(app)
        .delete(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
        .expect(404)

    const task = await Task.findOne(taskOne)
    expect(task).not.toBeNull()
})
